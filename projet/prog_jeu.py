# Conception d'un jeu sous forme textuel
# Le jeu fonctionne avec des fonctions essentiellement.

# Modules
import random


# Définition et inititialisation des variables

PV1=50 # points de vie du joueur 1
PV2=50 # points de vie du joueur 2
NBPOT=3 # nombre de potions du joueur 1
MENU = ["1","2"]
B=True

# Fonction d'attaque
def attaque(PV1,PV2):
    PVIE1=PV1
    PVIE2=PV2
    PP1=random.randint(5,15)
    PP2=random.randint(5,10)
    PVIE1=PVIE1-PP1
    PVIE2=PVIE2-PP2
    return PVIE1, PVIE2

# Fonction de potion
def potion(PV1, NBPOT):
    PVIE1=PV1    
    NB=NBPOT
    if NB == 0:
        print("Vous n'avez plus de potions. Vous devez attaquer. Recommencez")
        return PVIE1, NB
    else:
        PG=random.randint(15,50)
        PP1=random.randint(5,15)
        PVIE1=PVIE1+PG-PP1
        NB=NB-1
        return PVIE1, NB

while B == True:
    print("Choisissez votre action")
    print("1 - Attaquez votre adversaire : tapez 1")
    print("2 - Prenez une potion : tapez 2")
    A = input("Quelle action choisissez-vous ? :  ")
    if A not in MENU:
        print("Veuillez effectuer un choix valide : 1 ou 2")
    elif A=="1":
        PV1, PV2 = attaque(PV1, PV2)
        print(f"Il vous reste {PV1} points de vie")
    else:
        PV1, NBPOT = potion(PV1, NBPOT)
        print(f"Il vous reste {PV1} points de vie et {NBPOT} potions")        
    if PV1 <= 0 or PV2 <= 0:
        B=False

if PV1 <= 0:
    print("Vous avez perdu")
else:
    print("Vous avez gagné")









