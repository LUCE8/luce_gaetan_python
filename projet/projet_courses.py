# Projet Liste de course

course=[]
i=0
nb=[1,2,3,4,5]

print("Gestion de la liste de courses")
print("1 - Ajouter un article dans la liste de courses")
print("2 - Retirer un artiste de la liste de courses")
print("3 - Afficher la liste de courses")
print("4 - Vider la liste de courses")
print("5 - Quitter la liste de courses")

while i != 5:
    i=int(input("Entrez votre choix :  "))
    if i not in nb:
        print("Entrez un entier entre 1 et 5") 
    elif i==5:
        print("Au revoir")
    elif i == 1:
        A=input("Entrez un article :  ")
        if A in course:
            print("Cet article est déjà présent dans la liste")
        else:
            course.append(A)
    elif i==2:
        B=input("Entrez le nom de l'article à retirer de la liste de course :  ")
        if B not in course:
            print(f"L'article {B} n'est pas dans la liste")
        else:
            course.remove(B)
    elif i==3:
        if course==0:
            print("La liste de course est vide")
        else:
            print(f"La liste de courses est la suivante : {course}")
    else:
        course=[]






