# Ce programme propose à l'utilisateur de saisir son âge et lui indique s'il est mineur ou majeur
# Pas de boucle while pour cet exercice; on ne demande qu'une fois son âge à l'utilisateur

print("Ce programme vous demande de saisir votre âge et vous indique si vous êtres mineur ou majeur")
A=int(input("Saisissez votre âge :  "))
if A >= 18:
    print("Vous êtes majeur")
else:
    print("Vous êtes mineur")
