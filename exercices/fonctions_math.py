# Fonction opérations mathématiques
i="o"

# Fonctions avec return
def addition(a,b):
    c=a+b
    return c

def soustraction(a,b):
    c=a-b
    return c

def multiplication(a,b):
    c=a*b
    return c

def division(a,b):
    c=a/b
    return c

# Fonctions sans return
def addition2(a,b):
    c=a+b

def soustraction2(a,b):
    c=a-b

def multiplication2(a,b):
    c=a*b

def division2(a,b):
    c=a/b

print("Ce programme vous permet d'effectuer des opérations arithmétiques sur deux nombres entrés au clavier")
print("A - Addition")
print("S - Soustractio")
print("M - Multiplication")
print("D - Division")

while i != "n":
    NB1=int(input("Entrez le premier nombre :  "))
    NB2=int(input("Entrez le second nombre :  "))
    op=input("choisissez l'opération arithmétique : A / S / M / D :  ")
    if op == "A":
        res=addition(NB1,NB2)
        print(f"Le résultat de l'addittion entre {NB1} et {NB2} est égal à {res}")
        res2=addition2(NB1,NB2)
        print(f"Le résultat de l'addittion entre {NB1} et {NB2} avec la fonction sans return est égal à {res2}")
    if op == "S":
        res=soustraction(NB1,NB2)
        print(f"Le résultat de la soustraction entre {NB1} et {NB2} est égal à {res}")
        res2=soustraction2(NB1,NB2)
        print(f"Le résultat de la soustraction entre {NB1} et {NB2} avec la fonction sans return est égal à {res2}")
    if op == "M":
        res=multiplication(NB1,NB2)
        print(f"Le résultat de la multiplication entre {NB1} et {NB2} est égal à {res}")
        res2=multiplication2(NB1,NB2)
        print(f"Le résultat de la multiplication entre {NB1} et {NB2} avec la fonction sans return est égal à {res2}")
    if op == "D":
        res=division(NB1,NB2)
        print(f"Le résultat de la division entre {NB1} et {NB2} est égal à {res}")
        res2=division2(NB1,NB2)
        print(f"Le résultat de la division entre {NB1} et {NB2} avec la fonction sans return est égal à {res2}")
    i=input("Voulez-vous continuer ? o / n :  ")


    

