# programme affichant le maximum de deux nombres

print("Ce programme demande à l'utilisateur de saisir deux nombres et affiche le maximum des deux")

i="o"

while i != "n":
    A=int(input("Saisir le premier nombre :  "))
    B=int(input("Saisir le second nombre :  "))
    if A==B:
        print(f"Les deux nombres saisis sont égaux")
    elif A<B:
        print(f"Le second nombre saisi {B} est supérieur au premier nombre saisi {A}")
    else:
        print("Le premier nombre saisi {A] est supérieur au second nombre saisi {B}")
    i=input("Voulez-vous continuer , o / n :  ")
print("A bientôt")
