# Programme nombre pair

print("Ce programme indique si le nombre entier saisi par l'utilisateur est pait ou impair")
i="o"

while i != "n":
    A=int(input("Saisir un nombre entier :  "))
    if A%2 == 0:
        print(f"Le nombre saisi {A} est pair")
    else:
        print(f"Le nombre saisi {A} est impair")
    i=input("Voulez-vous continuer : o / n  ")
print("A demain")
